package essaiJD;

import java.lang.Math;


public class MyClassWithoutComments {
	
	protected int myIntegerAttribute;
	protected String myStringAttribute;


	public int getMyIntegerAttribute() {
		return myIntegerAttribute;
	}


	public void setMyIntegerAttribute(int myInt) {
		this.myIntegerAttribute = myInt;
	}
	

	public String getMyStringAttribute() {
		return myStringAttribute;
	}


	public void setMyStringAttribute(String myStringAttribute) {
		this.myStringAttribute = myStringAttribute;
	}


	public MyClassWithoutComments(int myIntAttrb, String myStrAttrb) {
		this.setMyIntegerAttribute(myIntAttrb);
		this.setMyStringAttribute(myStrAttrb);
	}
	
	
	public MyClassWithoutComments() {
		this(42, "default attribute value set by the constructor");
	}
	
	
	public static void main(String[] args) {
		System.out.println("This is the main()");
	}
	

}

