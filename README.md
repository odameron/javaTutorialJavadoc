# javaTutorialJavadoc

A short presentation of javadoc's motivation, principles and usage.

# Todo

- [ ] add java example files
- [ ] switch from markdown to LaTeX and use the `minted` package for nicer code display?
